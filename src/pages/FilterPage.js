import React from 'react';
import '../index.scss';
import Filter from '../components/filter/Filter';

function FilterPage() {
	return (
		<div className="filter-page">
			<div className="filter-page__container container">
				<h1>Filter</h1>
				<Filter/>
			</div>
		</div>
	);
}

export default FilterPage;
