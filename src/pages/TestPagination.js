import React from 'react';
import '../index.scss';
import Pagination from '../components/pagination/pagination';

function TestPagination() {
	return (
		<div className="post-page">
			<div className="container">
				<h1>Страница с пагинацией</h1>
				<Pagination/>
			</div>
		</div>
	);
}

export default TestPagination;
