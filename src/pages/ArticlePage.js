import React from 'react';
import '../index.scss';
import Article from '../components/article/article';

function ArticlePage() {
	const [list, setList] = React.useState([]);

	React.useEffect(() => {
		fetch('/static/articles.json')
			.then(response => response.json())
			.then(data => {
				setList(data);
			});
	}, []);

	return (
		<div className="article-page">
			<div className="container">
				<h1>Страница для статей</h1>

				<div className="article-page__list">
					{
						list.length && list.map(card => <Article key={card.id} data={card}/>)
					}
				</div>
			</div>
		</div>
	);
}

export default ArticlePage;
