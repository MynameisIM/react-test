import React from 'react';
import { Link } from 'react-router-dom';
import '../index.scss';

function ErrorPage() {
	return (
		<div className="error-page">
			<h1>404 - страница не найдена</h1>
			<h2>Вернуться на главную?</h2>
			<Link to='/'>
				<span>На главную</span>
			</Link>
		</div>
	);
}

export default ErrorPage;
