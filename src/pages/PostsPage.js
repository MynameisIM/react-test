import React from 'react';
import '../index.scss';
import Post from '../components/post/Post';
import { Provider } from 'react-redux';
import thunk from "redux-thunk";
import {createStore, compose, applyMiddleware} from 'redux';
import rootReducers from '../reducers/rootReducers';

const store = createStore(rootReducers,
	compose(
		applyMiddleware(thunk),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
));

function PostsPage() {
	return (
		<div className="post-page">
			<div className="container">
				<h1>Страница с постами</h1>
				<Provider store={store}>
					<Post/>
				</Provider>
			</div>
		</div>
	);
}

export default PostsPage;
