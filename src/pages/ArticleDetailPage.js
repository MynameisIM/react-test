import React from 'react';
import '../index.scss';
import {Link} from 'react-router-dom';

function ArticleDetailPage(props) {
	const id = props.match.params.id;
	const [art, setArt] = React.useState({});
	React.useEffect(() => {
		fetch('/static/articles_list.json')
			.then(response => response.json())
			.then(list => {
				const object = list.find(el => el.id === id);
				if (object) {
					setArt(object);
				}
			})
	}, []);

	return (
		<div className="article-page">
			<div className="container">
				<h1>{ art.title }</h1>
				<div className="article-page__text">{ art.text }</div>
				<div className="article-page__date">{ art.date }</div>
				<Link to='/articles'>Вернуться к статьям</Link>
			</div>
		</div>
	);
}

export default ArticleDetailPage;
