import React from 'react';
import '../index.scss';
import Context from '../context';
import Cont from '../components/cont/cont';

function ContextPage() {
	React.useContext(Context);

	const log = () => {
		console.log('test');
	};

	return (
		<Context.Provider value={{ log }}>
			<div className="post-page">
				<div className="container">
					<h1>Страница для контекста</h1>
					<Cont/>
				</div>
			</div>
		</Context.Provider>
	);
}

export default ContextPage;
