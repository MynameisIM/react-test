import React from 'react';
import '../index.scss';
import '../components/todo/todolist.scss';
import TodoList from "../components/todo/TodoList";
import Context from "../context";
import Loader from "../components/loader/Loader";

const AddTodo = React.lazy(() => new Promise(resolve => {  // lazy загрузка
	setTimeout(() => {
		resolve(import('../components/todo/AddTodo'))
	}, 3000);
}));

function ToDo() {
	const [todos, setTodos] = React.useState([]);
	const [loading, setLoading] = React.useState(true);

	React.useEffect(() => {
		fetch('https://jsonplaceholder.typicode.com/todos?_limit=5')
			.then(response => response.json())
			.then(todos => {
				setTimeout(() => {
					setTodos(todos);
					setLoading(false);
				}, 2000);
			})
	}, []);

	function toggleTodo(id) {
		setTodos(todos.map(todo => {
			if (todo.id === id) {
				todo.completed = !todo.completed;
			}
			return todo;
		}));
	}

	function removeTodo(id) {
		setTodos(todos.filter(todo => todo.id !== id));
	}

	function addTodo(title) {
		setTodos(todos.concat([{
			title,
			id: Date.now(),
			completed: false
		}]))
	}

	return (
		<Context.Provider value={{ removeTodo }}>
			<div className="post-page">
				<div className="container">
					<h1>Страница с to-do</h1>
					<React.Suspense fallback={<Loader/>}> // обертка для lazy подгрузки
						<AddTodo onCreate={addTodo}/>
					</React.Suspense>
					{
						todos.length ? <TodoList todos={todos} onToggle={toggleTodo}/> : loading ? <Loader/>: <p>No todos</p>
					}
				</div>
			</div>
		</Context.Provider>
	);
}

export default ToDo;
