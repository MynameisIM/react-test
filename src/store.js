const store = {
	posts: [
		{
			userId: "u1",
			title: "Title u1 user",
			text: "Text from u1 user",
			id: 1
		},
		{
			userId: "u2",
			title: "Title u2 user",
			text: "Text from u2 user",
			id: 2
		},
		{
			userId: "u3",
			title: "Title u3 user",
			text: "Text from u3 user",
			id: 3
		},
		{
			userId: "u4",
			title: "Title u4 user",
			text: "Text from u4 user",
			id: 4
		}
	],
};

export default store;
