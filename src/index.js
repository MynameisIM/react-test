import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './index.scss';

import Header from './components/header/Header';
import MainPage from './pages/MainPage';
import ErrorPage from './pages/ErrorPage';
import FilterPage from './pages/FilterPage';
import Footer from './components/footer/Footer';
import PostsPage from './pages/PostsPage';
import ToDo from './pages/ToDo';
import TestPagination from './pages/TestPagination';
import ContextPage from './pages/ContextPage';
import FormPage from './pages/FormPage';
import FormStep from './components/form-step/FormStep';
import ArticlePage from './pages/ArticlePage';
import ArticleDetailPage from './pages/ArticleDetailPage';

class App extends React.Component {
	render() {
		return (
			<Router>
				<Header/>
				<Switch>
					<Route exact path='/' component={MainPage}/>
					<Route exact path='/catalog' component={FilterPage}/>
					<Route exact path='/posts' component={PostsPage}/>
					<Route exact path='/pagination' component={TestPagination}/>
					<Route exact path='/todo' component={ToDo}/>
					<Route exact path='/context' component={ContextPage}/>
					<Route exact path='/form' component={FormPage}/>
					<Route path='/form/:step' component={FormStep}/>
					<Route exact path='/articles' component={ArticlePage}/>
					<Route exact path='/articles/:id' component={ArticleDetailPage}/>
					<Route exact path='*' component={ErrorPage}/>
				</Switch>
				<Footer/>
			</Router>
		);
	}
}

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);
