export function addNewPost(value) {
	return {
		type: 'ADD_POST',
		payload: value,
	}
}

export function loadNewPost() {
	return dispatch => {
		return fetch('https://jsonplaceholder.typicode.com/posts?_limit=5')
			.then(response => response.json())
			.then(data => {
				dispatch({
					type: 'LOAD_POSTS',
					payload: data
				})
			})
	}
}
