import store from '../store';

function post(state = store, action) {
	switch (action.type) {
		case 'ADD_POST':
			return {
				...state,
				posts: state.posts.concat([action.payload]),
			};
		case 'LOAD_POSTS':
			return {
				...state,
				posts: state.posts.concat(action.payload),
			};
		default:
			return state;
	}
}

export default post;
