export function getData (url) {
	return fetch(url)
}

export function sendData (url, data) {
	return fetch(url, {
		method: 'POST',
		body: JSON.stringify(data),
	})
}
