import React from 'react';
import './select.scss';

export default class Select extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			checked: '',
			value: 'Выберите вариант',
			open: false
		}
	}

	openSelect() {
		this.setState({
			open: !this.state.open,
		});
	}

	change(event) {
		this.setState({
			open: !this.state.open,
			checked: event.target.dataset.value,
			value: event.target.textContent,
		});
	}

	render() {
		return (
			<div className={`select ${this.props.cls || ''} ${this.state.open ? 'open': ''}`}>
				<div className="select__title" onClick={() => this.openSelect()}>{this.state.value}</div>
				<div className="select__dropdown">
					<div className='select__option hide' data-value=''>Выберите вариант</div>
					<div className={`select__option ${this.state.checked === 'val1' ? 'checked': ''}`} onClick={(event) => this.change(event)} data-value='val1'>Value 1</div>
					<div className={`select__option ${this.state.checked === 'val2' ? 'checked': ''}`} onClick={(event) => this.change(event)} data-value='val2'>Value 2</div>
					<div className={`select__option ${this.state.checked === 'val3' ? 'checked': ''}`} onClick={(event) => this.change(event)} data-value='val3'>Value 3</div>
				</div>
			</div>
		);
	}
}
