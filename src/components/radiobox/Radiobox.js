import React from 'react';
import './radiobox.scss';

export default class Radiobox extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className={`radiobox ${this.props.cls || ''}`}>
				<input
					type='radio'
					value={this.props.value}
					name={`${this.props.name}`}
					className='radiobox__field'
					checked={this.props.checked}
					id={this.props.id}
					onChange={this.props.onChange}
				/>
				{this.props.id && this.props.label ? <label htmlFor={this.props.id}>{this.props.label}</label>: null}
			</div>
		);
	}
}
