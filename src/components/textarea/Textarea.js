import React from 'react';
import './textarea.scss';

export default class Textarea extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className={`textarea ${this.props.cls || ''}`}>
				<textarea
					placeholder={this.props.placeholder}
					value={this.props.value}
					name={`${this.props.name}`}
					className='textarea__field'
					id={this.props.id}
					onChange={this.props.onChange}
				/>
				{this.props.id && this.props.label ? <label htmlFor={this.props.id}>{this.props.label}</label>: null}
			</div>
		);
	}
}
