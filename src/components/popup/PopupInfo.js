import React from 'react';

function PopupInfo(props) {
	return (
		<div className='popup__box'>
			{props.info.title && <div className='popup__title'>{props.info.title}</div>}
			{props.info.text && <div className='popup__text'>{props.info.text}</div>}
		</div>
	);
}
export default PopupInfo;
