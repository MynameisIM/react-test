import React from 'react';
import './popup.scss';
import PopupInfo from './PopupInfo';
import Quiz from '../quiz/quiz';

export default class Popup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			close: false,
		};
	}

	closePopup() {
		if (this.props.closePopup) {
			this.setState({
				close: true
			});
			setTimeout(() => {
				this.props.closePopup();
			}, 500);
		}
	}

	render() {
		return (
			<div className={`popup ${this.state.close ? 'close': ''}`}>
				<div className="popup__layout" onClick={() => this.closePopup()}/>
				<div className={`popup__wrapper ${this.props.quiz ? 'popup__wrapper--quiz': ''}`}>
					<button className="popup__close" name='close popup' onClick={() => this.closePopup()}/>
					<div className="popup__content">
						{this.props.info ? <PopupInfo info={this.props.info}/>: this.props.quiz ? <Quiz/>: 'Text in popup'}
					</div>
				</div>
			</div>
		);
	}
}
