import React, {useState} from 'react';
import { connect } from 'react-redux';
import {addNewPost} from '../../actions';

function AddPost({ addNewPost, posts }) {
	const [text, setText] = useState('');
	const [title, setTitle] = useState('');

	const createNewPost  = () => {
		const len = posts.length;
		if (text.length && title.length) {
			addNewPost({
				userId: 'u5',
				title,
				text,
				id: len + 1,
			});
			setTitle('');
			setText('');
		}
	};
	return (
		<div className='add-post'>
			<input type="text" placeholder='Введите заголовок' onChange={(e) => setTitle(e.target.value)} value={title} />
			<textarea placeholder='Введите текст' onChange={(e) => setText(e.target.value)} value={text}/>
			<div>
				<button type='button' onClick={() => createNewPost()}>Отправить</button>
			</div>
		</div>
	)
}

const mapStateToProps = state => {
	return {
		placeholder: state.post.placeholder,
		posts: state.post.posts,
	}
};

const mapDispatchToProps = {
	addNewPost,
};


export default connect(mapStateToProps, mapDispatchToProps)(AddPost);
