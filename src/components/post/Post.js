import React from 'react';
import './post.scss';
import AddPost from './AddPost';
import Posts from "./posts";

function Post() {
	return (
		<div className='post'>
			<div className="post-title">Add new post</div>
			<AddPost/>
			<Posts/>
		</div>
	)
}

export default Post;
