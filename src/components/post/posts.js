import React from "react";
import { connect } from "react-redux";
import {loadNewPost} from '../../actions';

function Posts({ posts, loadNewPost }) {
	return (
		<div className='posts'>
			{ posts.length ? posts.map(post => {
				return (
					<div className='post-item' key={post.title+post.id}>
						<div className='post-item__user'>User with id: {post.userId}</div>
						<div className='post-item__title'>{post.title}</div>
						<div className='post-item__text'>{post.text}</div>
					</div>
				)
			}) : null }
			<div className='posts__footer'>
				<button onClick={() => loadNewPost()}>Загрузить с сервера</button>
			</div>
		</div>
	);
}

const mapStateToProps = state => ({
	posts: state.post.posts
});

const mapDispatchToProps = {
	loadNewPost,
};


export default connect(mapStateToProps, mapDispatchToProps)(Posts);
