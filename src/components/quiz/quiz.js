import React from 'react';
import './quiz.scss';

function Quiz() {
	const [questions, setQuestions] = React.useState([]);
	const [step, setStep] = React.useState(0);

	React.useEffect(() => {
		fetch('/static/quiz.json').then(response => response.json()).then(list => {
			setQuestions(list);
		});
	}, []);

	function updateStep(next, { name, value }) {
		if (next) {
			setStep(step + 1);
			console.log(name, value);
		} else {
			setStep(step - 1);
		}
	}

	function resetQuiz() {
		setStep(0);
	}

	console.log(questions, step);

	return (
		<div className='quiz'>
			{ questions.length && (
				<React.Fragment>
					<div className="quiz__steps">
						{ questions.length && questions.map((_, index) => <div className={`quiz__step ${index + 1 <= step ? 'active': ''}`} key={index}>{ index + 1 }</div>) }
						<div className={`quiz__step ${step === questions.length ? 'active': ''}`}>Result</div>
					</div>
					<div className="quiz__content">
						{ questions[step] ? (
							<React.Fragment>
								<div className="quiz__content-title">{ questions[step].title || '' }</div>
								<div className='quiz__content-wrapper'>
									{ questions[step].list && questions[step].list.map((item, idx) => {
										return (
											<div className='quiz__card' onClick={() => updateStep(true, item)} key={idx+item.name+item.value} data-name={item.name} data-value={item.value}>
												<div className="quiz__card-label">{item.label || ''}</div>
												<img src={item.image} alt="card"/>
											</div>
										)
									}) }
								</div>
								{ step > 0 && <button className='quiz__content-back' onClick={() =>  console.log('ToDo')/*updateStep(false, item)*/}>Back</button> }
							</React.Fragment>
						) : (
							<React.Fragment>
								<p>Result box</p>
								<button className='quiz__content-back' onClick={() => resetQuiz()}>Try again</button>
							</React.Fragment>
						) }
					</div>
				</React.Fragment>
			) }
		</div>
	);
}
export default Quiz;
