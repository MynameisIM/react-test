import React from 'react';
import './main.scss';
import Slider from "../slider/Slider";
import Form from "../form/Form";
import Accordion from '../accordion/Accordion';
import Tabs from '../tabs/Tabs';
import YandexMap from '../map/YandexMap';
import Popup from '../popup/Popup';
import List from "../list/List";

export default class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			slides: [
				{
					src: './static/images/slide1.jpg',
					title: 'Заголовок первого слайда',
					text: 'Текст первого слайда Текст первого слайда Текст первого слайда'
				},
				{
					src: './static/images/slide2.jpg',
					title: 'Заголовок второго слайда',
					text: 'Текст второго слайда Текст второго слайда Текст второго слайда'
				},
				{
					src: './static/images/slide3.jpg',
					title: 'Заголовок третьего слайда',
					text: 'Текст третьего слайда Текст третьего слайда Текст третьего слайда'
				}
			],
			accordion: [
				{
					"title": "title",
					"text": "dropdown text",
					"open": false
				},
				{
					"title": "title 2",
					"text": "dropdown text 2",
					"open": false
				},
				{
					"title": "title 3",
					"text": "dropdown text 3",
					"open": false
				}
			],
			defaultMap: {
				center: [55.751574, 37.573856],
				zoom: 5,
			},
			showPopup: false,
			showQuizPopup: false,
		};
	}

	switchPopup() {
		this.setState({
			showPopup: !this.state.showPopup,
		});
	}

	switchQuizPopup() {
		this.setState({
			showQuizPopup: !this.state.showQuizPopup,
		});
	}

	render() {
		return (
			<div className='main'>
				<div className="main__content">
					<div className="container">
						<Slider slides={this.state.slides}/>
						<div className="main__form">
							<div className="main__form-title">Форма заявки</div>
							<Form method='get'/>
						</div>
						<div className="main__accordion">
							<div className="main__form-title">Аккордион</div>
							<Accordion data={this.state.accordion} animate={true}/>
						</div>
						<div className="main__tabs">
							<div className="main__form-title">Табы</div>
							<Tabs />
						</div>
						<div className="main__map">
							<div className="main__form-title">Карта</div>
							<YandexMap defaultState={this.state.defaultMap}/>
						</div>
						<div className="main__modal">
							<div className="main__form-title">Модальные окна</div>
							<button className='main__modal-button' onClick={() => this.switchPopup()}>Открыть попап</button>
							{this.state.showPopup && <Popup closePopup={() => this.switchPopup()}/>}
							<button className='main__modal-button' onClick={() => this.switchQuizPopup()}>Quiz</button>
							{this.state.showQuizPopup && <Popup quiz={true} closePopup={() => this.switchQuizPopup()}/>}
						</div>
						<div className="main__list">
							<div className="main__form-title">Список карточек</div>
							<List/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
