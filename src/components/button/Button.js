import React from 'react';
import './button.scss';

export default class Button extends React.Component {
	constructor(props) {
		super(props);
	}

	btnClick() {
		if (this.props.clickHandler) {
			this.props.clickHandler();
		}
	}

	render() {
		return (
			<button
				type={this.props.type || 'button'}
				name={this.props.name}
				className={`button ${this.props.cls}`}
				onClick={() => this.btnClick()}
			>
				{this.props.text}
			</button>
		);
	}
}
