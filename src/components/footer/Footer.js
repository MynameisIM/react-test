import React from 'react';
import './footer.scss';
import Svg from "../svg/Svg";

export default function Footer () {
	return (
		<div className='footer'>
			<div className="footer__wrapper container">
				<Svg name='#logo-svg' cls='svg--footer-logo'/>
			</div>
		</div>
	);
};
