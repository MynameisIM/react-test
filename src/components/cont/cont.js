import React from 'react';
import Context from '../../context';

function Cont() {
	const { log } = React.useContext(Context);
	return (
		<div className="cont">
			<div className="container">
				<h1 onClick={() => log()}>test component</h1>
			</div>
		</div>
	);
}

export default Cont;
