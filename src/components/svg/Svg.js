import React from 'react';
import './svg.scss';
import sprite from '../../svg/sprite.svg';

function Svg(props) {
	return (
		<svg className={`svg ${props.cls ? props.cls: ''}`}>
			<use href={sprite + props.name}/>
		</svg>
	)
}

export default Svg;
