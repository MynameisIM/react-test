import React from 'react';
import './header.scss';
import Logo from '../../components/logo/Logo';
import Nav from '../nav/Nav';
import Auth from '../auth/Auth';

export default function Header () {
	return (
		<header className='header'>
			<div className="header__wrapper container">
				<Logo svg='#logo-svg'/>
				<Nav/>
				<Auth/>
			</div>
		</header>
	);
};
