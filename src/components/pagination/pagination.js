import React from 'react';
import './pagination.scss';

export default class Pagination extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			cards: [
				{
					id: 1,
					title: "1"
				},
				{
					id: 2,
					title: "2"
				},
				{
					id: 3,
					title: "3"
				},
				{
					id: 4,
					title: "4"
				},
				{
					id: 5,
					title: "5"
				},
				{
					id: 6,
					title: "6"
				},
				{
					id: 7,
					title: "7"
				},
				{
					id: 8,
					title: "8"
				},
				{
					id: 9,
					title: "9"
				},
				{
					id: 10,
					title: "10"
				},
				{
					id: 11,
					title: "11"
				},
				{
					id: 12,
					title: "12"
				},
				{
					id: 13,
					title: "13"
				},
				{
					id: 14,
					title: "14"
				},
				{
					id: 15,
					title: "15"
				},
				{
					id: 16,
					title: "16"
				}
			],
			filtered: [],
			page: 1,
			count: 4
		};
	}

	componentDidMount() {
		this.updateFilteredCards(this.state.page);
	}

	updateFilteredCards(page) {
		const from = (page - 1) * this.state.count;
		const to = this.state.count * page;
		const arr = this.state.cards.slice(from, to);

		this.setState({
			filtered: arr,
			page,
		});
	}

	renderCards() {
		return this.state.filtered.map(card => <div key={card.id} className='pagination__card'>{card.title}</div>);
	}

	changePage(pg) {
		this.updateFilteredCards(pg);
	}

	renderPagination() {
		const count = Math.ceil(this.state.cards.length / this.state.count);
		const list = Array.from(Array(count).keys()).map(n => ++n);

		return list.map((el, index) =>
			<div
				key={index}
				className={`pagination__item ${this.state.page === el ? 'active': ''}`}
				onClick={() => this.changePage(el)}>{el}
			</div>
		)
	}

	render() {
		return (
			<div className='pagination'>
				<div className="pagination__cards">
					{this.renderCards()}
				</div>
				<div className="pagination__control">
					{this.renderPagination()}
				</div>
			</div>
		);
	}
}
