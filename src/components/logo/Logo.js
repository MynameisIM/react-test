import React from 'react';
import { Link } from 'react-router-dom';
import './logo.scss';
import Svg from "../svg/Svg";

export default function Logo ({ svg }) {
	return (
		<Link to='/'>
				<span className='logo'>
					<Svg name={svg} cls='svg--header-logo'/>
				</span>
		</Link>
	);
};
