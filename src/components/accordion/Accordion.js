import React from 'react';
import './accordion.scss';

export default class Accordion extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			list: this.props.data || []
		};
	}

	btnClick(event, index) {
		const item = event.target.parentElement;
		if (item) {
			const dropdown = item.querySelector('.accordion__dropdown-box');
			if (dropdown) {
				if (dropdown.classList.contains('animate')) {
					const height = dropdown.scrollHeight;
					if (dropdown.style.maxHeight === '') {
						dropdown.style.maxHeight = `${height}px`;
						item.classList.toggle('open');
					} else {
						dropdown.removeAttribute('style');
						item.classList.toggle('open');
					}
				} else {
					item.classList.toggle('open');
				}
			}
		}
	}

	renderItems() {
		if (this.state.list.length) {
			return this.state.list.map((el, index) => {
				return (
					<div className={`accordion__item ${el.open ? 'open': ''}`} key={index}>
						<div className="accordion__header" onClick={(event) => this.btnClick(event, index)}>{el.title}</div>
						<div className={`accordion__dropdown-box ${this.props.animate ? 'animate': ''}`}>
							<div className='accordion__dropdown'>{el.text}</div>
						</div>
					</div>
				);
			})
		}
	}

	render() {
		return (
			<div className='accordion'>
				{this.renderItems()}
			</div>
		);
	}
}
