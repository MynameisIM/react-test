import React from 'react';
import './form.scss';
import Input from '../input/Input';
import Textarea from '../textarea/Textarea';
import Button from '../button/Button';
import Popup from "../popup/Popup";

export default class Form extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: {
				name: {
					view: 'input',
					name: 'name',
					placeholder: 'Имя',
					value: '',
					mode: 'half',
					valid: true,
					required: true,
					validate: 'name'
				},
				phone: {
					view: 'input',
					type: 'tel',
					name: 'phone',
					placeholder: 'Телефон',
					value: '',
					mode: 'half',
					valid: true,
					required: true,
					validate: 'phone'
				},
				email: {
					view: 'input',
					type: 'email',
					name: 'email',
					value: '',
					valid: true,
					required: true,
					placeholder: 'Email',
					validate: 'email'
				},
				message: {
					view: 'textarea',
					name: 'message',
					placeholder: 'Сообщение',
					value: '',
					valid: true,
				}
			},
			popup: {
				show: false,
				title: 'Спасибо',
				text: 'Специалист свяжется с Вами'
			}
		};
		this.renderFields = this.renderFields.bind(this);
		this.fieldChange = this.fieldChange.bind(this);
		this.submit = this.submit.bind(this);
		this.resetForm = this.resetForm.bind(this);
	}

	switchPopup() {
		const popup = this.state.popup;
		popup.show = true;

		this.setState({
			popup,
		});
	}

	handleSubmit(e) {
		e.preventDefault();
		this.submit(this.state.fields);
	}

	submit(obj) {
		const data = obj;

		Object.keys(obj).forEach((key) => {
			const object = obj[key];
			if ('required' in object) {
				if ((object.validate === 'name' && !/[a-zA-Zа-яА-Я]{2,}/g.test(object.value)) ||
					(object.validate === 'phone' && !/\+?\d{6,12}/g.test(object.value)) ||
					(object.validate === 'email' && !/\w{3,}@\w{4,}\.\w{2,}/g.test(object.value))) {
					object.valid = false;
				} else {
					object.valid = true;
				}
			}
		});

		this.setState({
			fields: data,
		});
		const allFieldsIsValid = Object.values(data).every(field => field.valid);
		if (allFieldsIsValid) {
			const formateData = {};
			Object.keys(data).forEach((key) => {
				formateData[key] = data[key].value;
			});
			console.log('Отправлены данные: ', formateData);
			this.resetForm(data);

			const popup = this.state.popup;
			popup.show = true;
			this.setState({
				popup,
			});
		}
	}

	resetForm(obj) {
		Object.values(obj).forEach((el) => {
			el.value = '';
			el.valid = true;
		});
		this.setState({
			fields: obj,
		});
	}

	fieldChange(event, fieldName) {
		const copyState = {...this.state.fields};
		copyState[fieldName].value = event.target.value;

		this.setState({
			fields: copyState,
		});
	}

	renderFields() {
		return Object.keys(this.state.fields).map((elem, index) => {
			const field = this.state.fields[elem];
			return (
				<React.Fragment key={index}>
					{
						field.view === 'input' ?
							<Input
								name={field.name}
								placeholder={field.placeholder}
								value={field.value}
								onChange={event => this.fieldChange(event, elem)}
								cls={`form__element ${field.mode ? 'form__element--'+field.mode: ''}`}
								isValid={field.valid}
							/>:
						field.view === 'textarea' ?
							<Textarea
								name={field.name}
								placeholder={field.placeholder}
								value={field.value}
								onChange={event => this.fieldChange(event, elem)}
								cls={`form__element ${field.mode ? 'form__element--'+field.mode: ''}`}
								isValid={field.valid}
							/>:
							null
					}
				</React.Fragment>
			);
		});
	}

	render() {
		return (
			<form action={this.props.action || '/'} method={this.props.method || 'POST'} className='form' onSubmit={event => this.handleSubmit(event)}>
				<div className="form__wrapper">
					{this.renderFields()}
					<Button type='submit' name='submit form' text='Отправить форму' cls='form__element form__element--submit'/>
				</div>
				{this.state.popup.show && <Popup info={this.state.popup} closePopup={() => this.switchPopup()}/>}
			</form>
		);
	}
}
