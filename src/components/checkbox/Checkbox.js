import React from 'react';
import './checkbox.scss';

export default class Checkbox extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className={`checkbox ${this.props.cls || ''}`}>
				<input
					type='checkbox'
					value={this.props.value}
					name={`${this.props.name}`}
					className='checkbox__field'
					checked={this.props.checked}
					id={this.props.id}
					onChange={this.props.onChange}
				/>
				{this.props.id && this.props.label ? <label htmlFor={this.props.id}>{this.props.label}</label>: null}
			</div>
		);
	}
}
