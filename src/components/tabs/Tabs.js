import React from 'react';
import './tabs.scss';
import Footer from '../footer/Footer';

export default class Tabs extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			tabs: [
				{
					"title": "tab 1",
					"content": "content 1",
					"active": true
				},
				{
					"title": "tab 2",
					"content": Footer
				},
				{
					"title": "tab 3",
					"content": "content 3"
				},
				{
					"title": "tab 4",
					"content": "content 4"
				}
			]
		}
	}

	renderContent() {
		const obj = this.state.tabs.find(o => o.active);
		const TagName = obj.content;
		return (
			<div className='tabs__content'>
				{typeof obj.content === 'function' ? <TagName/>: `${obj.content}`}
			</div>
		);
	}

	switchTabs(index) {
		const newState = this.state.tabs;
		newState.forEach(s => s.active = false);
		newState[index].active = true;

		this.setState({
			tabs: newState
		});
	}

	render() {
		return (
			<div className="tabs">
				<div className="tabs__buttons">
					{this.state.tabs.map((elem, index) => {
						return (
							<button className={`tabs__tab ${elem.active ? 'active': ''}`} key={elem.title+index} onClick={() => this.switchTabs(index)}>{elem.title}</button>
						);
					})}
				</div>
				<div className="tabs__content-box">
					{this.renderContent()}
				</div>
			</div>
		);
	}
}
