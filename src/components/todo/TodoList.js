import React from 'react';
import PropTypes from 'prop-types';
import './todolist.scss';
import TodoItem from "../todo/TodoItem";

function TodoList({ todos, onToggle }) {
	return (
		<ul className='todo-list'>
			{ todos.map((item, index) => <TodoItem
				key={item.id}
				index={index}
				todo={item}
				onChange={onToggle}
			/>) }
		</ul>
	)
}

TodoList.propTypes = {
	todos: PropTypes.array,
	onToggle: PropTypes.func.isRequired,
};

export default TodoList;
