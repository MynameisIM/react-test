import React from 'react';
import PropTypes from 'prop-types';
import '../todo/todoitem.scss';
import Context from "../../context";

function TodoItem({ todo, index, onChange }) {
	const { removeTodo } = React.useContext(Context);
	const classes = [];
	if (todo.completed) {
		classes.push('done');
	}
	return (
		<li className='todo-item'>
			<span className={classes.join(' ')}>
				<input type="checkbox" onChange={() => onChange(todo.id)} checked={todo.completed}/>
				<strong>{index + 1}</strong>{ todo.title }
			</span>
			<button onClick={() => removeTodo(todo.id)}>&times;</button>
		</li>
	)
}

TodoItem.propTypes = {
	index: PropTypes.number,
	todo: PropTypes.object.isRequired,
	onChange: PropTypes.func.isRequired,
};

export default TodoItem;
