import React from 'react';
import PropTypes from 'prop-types';

function useInputValue(defaultValue = '') { // создание собственного хука
	const [value, setValue] = React.useState('');
	return {
		bind: {
			value,
			onChange: event => setValue(event.target.value)
		},
		clear: () => setValue(''),
		value: () => value
	}
}

function AddTodo({ onCreate }) {
	const input = useInputValue(''); // переменная для собственного хука

	function submitHandler(event) {
		event.preventDefault();

		if (input.value().trim()) {
			onCreate(input.value());
			input.clear();  // очистка поля собственным хуком
		}
	}

	return ( // передача собственного хука {...input}
		<form className='add-todo' onSubmit={submitHandler}>
			<input type="text" {...input.bind} />
			<button type='submit'>Add todo</button>
		</form>
	)
}

AddTodo.propTypes = {
	onCreate: PropTypes.func.isRequired,
};

export default AddTodo;
