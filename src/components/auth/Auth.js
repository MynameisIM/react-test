import React from 'react';
import './auth.scss';
import Svg from "../svg/Svg";

export default class Auth extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			auth: false,
			name: 'Игорь',
			dropdownVisible: false,
		};
		this.toggleAuth = this.toggleAuth.bind(this);
		this.showDropdown = this.showDropdown.bind(this);
	}

	toggleAuth() {
		this.setState(() => ({
			auth: !this.state.auth,
			dropdownVisible: false,
		}));
	}

	showDropdown() {
		this.setState(() => ({
			dropdownVisible: !this.state.dropdownVisible,
		}));
	}

	render() {
		return (
			<div className='auth'>
				<div className="auth__header" onClick={this.showDropdown}>
					<Svg name='#login' cls='svg--login'/>
					<span>{this.state.auth ? this.state.name: 'Личный кабинет'}</span>
				</div>
				{
					this.state.dropdownVisible ? (
						<div className="auth__dropdown">
							<button className='auth__button' onClick={this.toggleAuth}>{this.state.auth ? 'Выйти': 'Войти'}</button>
						</div>
					) : null
				}
			</div>
		);
	}
}
