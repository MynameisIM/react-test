import React from 'react';
import './article.scss';
import { Link } from 'react-router-dom';

function Article({ data }) {
	return (
		<div className='article'>
			<div className="article__title">{ data.title }</div>
			<div className="article__text">{ data.text }</div>
			<div className="article__date">{ data.date }</div>
			<Link to={`/articles/${data.id}`}>Перейти к статье</Link>
		</div>
	)
}

export default Article;
