import React from 'react';
import './card.scss';

export default class Card extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className={`card ${this.props.cls ? this.props.cls : ''}`}>
				<div className="card__image-box">
					<img src={this.props.data.src} alt={this.props.data.name}/>
				</div>
				<div className="card__info">
					<div className="card__name">{this.props.data.name}</div>
					<div className="card__footage">метраж: <span>{this.props.data.footage}</span> м²</div>
					<div className="card__price">цена: <span>{this.props.data.price}</span></div>
					<div className="card__available">доступность: <span>{this.props.data.available ? 'Доступен': 'Не доступен'}</span></div>
					<div className="card__deal">тип сделки: <span>{this.props.data.deal === '1' ? 'Покупка': 'Аренда'}</span></div>
				</div>
			</div>
		);
	}
}
