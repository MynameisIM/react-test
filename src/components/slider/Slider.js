import React from 'react';
import SwiperCore, { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import './slider.scss';

SwiperCore.use([Navigation, Pagination]);

function Slider(props) {
	const list = props.slides;
	return (
		<Swiper spasebetween={10} navigation pagination={{clickable: true, bulletActiveClass: 'slide__bullet--active'}} className='slider'>
			{list.length ? (
				list.map((slide, index) => (
					<SwiperSlide key={slide.text + index}>
						<div className="slide">
							<img src={slide.src} alt="alt"/>
							<div className="slide__text-box">
								{slide.title? <div className="slide__title">{slide.title}</div>: null}
								<div className="slide__text">{slide.text}</div>
							</div>
						</div>
					</SwiperSlide>
				))
			): null}
		</Swiper>
	);
}

export default Slider;
