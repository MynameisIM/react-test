import React from 'react';
import './map.scss';
import { YMaps, Map, Placemark, Clusterer } from 'react-yandex-maps';
import markers from './markers';

export default class YandexMap extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			markers: markers,
			clicked: ''
		}
	}

	placemarkClick(marker) {
		const { name } = marker;
		this.setState({
			clicked: name,
		})
	}

	render() {
		return (
			<div className="map">
				<div className="map__box">
					<YMaps>
						<Map defaultState={this.props.defaultState} width='100%' height='100%'>
							<Clusterer>
								{
									this.state.markers && this.state.markers.length ?
										this.state.markers.map((marker, index) =>
											<Placemark
												key={marker.name + index}
												geometry={marker.coords}
												onClick={() => this.placemarkClick(marker)}
											/>
										): null
								}
							</Clusterer>
						</Map>
					</YMaps>
				</div>
				<div className="map__info">
					<h5>Нажат маркер со следующим именем:</h5>
					<h5>{this.state.clicked}</h5>
				</div>
			</div>
		);
	}
}
