import React from 'react';
import Input from '../input/Input';
import './filter.scss';
import Checkbox from '../checkbox/Checkbox';
import Radiobox from "../radiobox/Radiobox";
import Select from "../select/Select";
import Button from "../button/Button";
import Card from "../card/Card";

export default class Filter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: {
				price_from: {
					name: 'price_from',
					placeholder: 'Цена от: ',
					value: '',
					mode: 'quarter'
				},
				price_to: {
					name: 'price_to',
					placeholder: 'Цена до: ',
					value: '',
					mode: 'quarter'
				},
				footage: {
					name: 'footage',
					placeholder: 'Укажите метраж',
					value: '',
					mode: 'quarter'
				},
				rooms: {
					name: 'rooms',
					placeholder: 'Укажите количество комнат',
					value: '',
					mode: 'quarter'
				},
				available: {
					name: 'available',
					label: 'Только доступные',
					value: 'available',
					id: 'available-checkbox',
					mode: 'quarter',
					checkbox: true,
					checked: false
				},
				deal: {
					name: 'deal',
					values: ['1', '2'],
					checked: '1',
					mode: 'radio'
				},
				select: {
					mode: 'quarter',
					value: '_null'
				}
			},
			default_fields: {
				price_from: {
					name: 'price_from',
						placeholder: 'Цена от: ',
						value: '',
						mode: 'quarter'
				},
				price_to: {
					name: 'price_to',
						placeholder: 'Цена до: ',
						value: '',
						mode: 'quarter'
				},
				footage: {
					name: 'footage',
						placeholder: 'Укажите метраж',
						value: '',
						mode: 'quarter'
				},
				rooms: {
					name: 'rooms',
						placeholder: 'Укажите количество комнат',
						value: '',
						mode: 'quarter'
				},
				available: {
					name: 'available',
						label: 'Только доступные',
						value: 'available',
						id: 'available-checkbox',
						mode: 'quarter',
						checkbox: true,
						checked: false
				},
				deal: {
					name: 'deal',
						values: ['1', '2'],
						checked: '1',
						mode: 'radio'
				},
				select: {
					mode: 'quarter',
						value: '_null'
				}
			},
			cards: [
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '2 350 000',
					footage: '130',
					available: true,
					deal: '1',
					id: 'card1'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '1 650 000',
					footage: '46',
					available: true,
					deal: '2',
					id: 'card2'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '4 350 000',
					footage: '170',
					available: true,
					deal: '2',
					id: 'card3'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '6 350 000',
					footage: '220',
					available: false,
					deal: '1',
					id: 'card4'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '2 850 000',
					footage: '130',
					available: false,
					deal: '1',
					id: 'card5'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '1 950 000',
					footage: '46',
					available: false,
					deal: '2',
					id: 'card6'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '8 990 000',
					footage: '170',
					available: false,
					deal: '2',
					id: 'card7'
				},
				{
					src: './static/images/room.jpg',
					name: 'Карточка с товаром',
					price: '16 450 000',
					footage: '220',
					available: false,
					deal: '1',
					id: 'card8'
				}
			],
			filteredCards: [],
		};
	}

	elemChange(event, name) {
		const obj = this.state.fields;
		if (name in obj) {
			if (event.target.type === 'checkbox') {
				obj[name].checked = event.target.checked;
			} else if (event.target.type === 'radio') {
				obj[name].checked = event.target.value;
			} else {
				obj[name].value = event.target.value;
			}

			this.setState({
				fields: obj,
			});
		}

		this.submitHandler();
	}

	submitHandler(event) {
		if (event) {
			event.preventDefault();
		}
		const copyState = { ...this.state.fields };
		const data = {};
		Object.keys(copyState).forEach((key) => {
			if (!data[key]) {
				if (copyState[key].values) {
					data[key] = this.clearSpaces(copyState[key].checked);
				} else if (copyState[key].checkbox) {
					data[key] = copyState[key].checked;
				} else {
					data[key] = this.clearSpaces(copyState[key].value);
				}
			}
		});
		this.filterResults(data);
	}

	clearSpaces(data) {
		return String(data).replace(/\s/g, '');
	}

	componentDidMount() {
		this.submitHandler();
	}

	filterResults(data) {
		const arr = this.state.cards.filter((card) => {
			return (
				(data.available ? card.available === data.available: true) &&
				(card.deal === data.deal) &&
				(Number(this.clearSpaces(card.price)) >= Number(this.clearSpaces(data.price_from))) &&
				(data.price_to !== '' ? Number(this.clearSpaces(card.price)) <= Number(this.clearSpaces(data.price_to)): true)
			)
		});
		this.setState({
			filteredCards: arr,
		});
	}

	renderCards(cards) {
		return cards.map(card => <Card data={card} key={card.id} cls='filter__card'/>);
	}

	resetHandler() {
		this.setState({
			fields: this.state.default_fields,
		});
	}

	render() {
		return (
			<form action='/' className='filter' onSubmit={(event) => this.submitHandler(event)}>
				<div className="filter__wrapper">
					<div className="filter__elems">
						<Input
							name={this.state.fields.price_from.name}
							placeholder={this.state.fields.price_from.placeholder}
							value={this.state.fields.price_from.value}
							onChange={(event) => this.elemChange(event, this.state.fields.price_from.name)}
							cls={`filter__element ${this.state.fields.price_from.mode ? 'filter__element--'+this.state.fields.price_from.mode: ''}`}
							isValid={true}
						/>
						<Input
							name={this.state.fields.price_to.name}
							placeholder={this.state.fields.price_to.placeholder}
							value={this.state.fields.price_to.value}
							onChange={(event) => this.elemChange(event, this.state.fields.price_to.name)}
							cls={`filter__element ${this.state.fields.price_to.mode ? 'filter__element--'+this.state.fields.price_to.mode: ''}`}
							isValid={true}
						/>
						<Input
							name={this.state.fields.footage.name}
							placeholder={this.state.fields.footage.placeholder}
							value={this.state.fields.footage.value}
							onChange={(event) => this.elemChange(event, this.state.fields.footage.name)}
							cls={`filter__element ${this.state.fields.footage.mode ? 'filter__element--'+this.state.fields.footage.mode: ''}`}
							isValid={true}
						/>
						<Input
							name={this.state.fields.rooms.name}
							placeholder={this.state.fields.rooms.placeholder}
							value={this.state.fields.rooms.value}
							onChange={(event) => this.elemChange(event, this.state.fields.rooms.name)}
							cls={`filter__element ${this.state.fields.rooms.mode ? 'filter__element--'+this.state.fields.rooms.mode: ''}`}
							isValid={true}
						/>
						<Checkbox
							label={this.state.fields.available.label}
							name={this.state.fields.available.name}
							value={this.state.fields.available.value}
							checked={this.state.fields.available.checked}
							id={this.state.fields.available.id}
							onChange={(event) => this.elemChange(event, this.state.fields.available.name)}
							cls={`filter__element ${this.state.fields.available.mode ? 'filter__element--'+this.state.fields.available.mode: ''}`}
						/>
						<Radiobox
							type='radio'
							value={this.state.fields.deal.values[0]}
							label='Купить'
							name={this.state.fields.deal.name}
							className='radiobox__field'
							checked={this.state.fields.deal.checked === '1'}
							id='deal_1'
							cls={`filter__element ${this.state.fields.deal.mode ? 'filter__element--'+this.state.fields.deal.mode: ''}`}
							onChange={(event) => this.elemChange(event, this.state.fields.deal.name)}
						/>
						<Radiobox
							type='radio'
							value={this.state.fields.deal.values[1]}
							label='Арендавать'
							name={this.state.fields.deal.name}
							className='radiobox__field'
							checked={this.state.fields.deal.checked === '2'}
							id='deal_2'
							cls={`filter__element ${this.state.fields.deal.mode ? 'filter__element--'+this.state.fields.deal.mode: ''}`}
							onChange={(event) => this.elemChange(event, this.state.fields.deal.name)}
						/>
						<Select
							cls={`filter__element ${this.state.fields.select.mode ? 'filter__element--'+this.state.fields.select.mode: ''}`}
						/>
						{/*<RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>*/}
						{/*	<FormControlLabel value="female" control={<Radio />} label="Female" />*/}
						{/*	<FormControlLabel value="male" control={<Radio />} label="Male" />*/}
						{/*	<FormControlLabel value="other" control={<Radio />} label="Other" />*/}
						{/*	<FormControlLabel value="disabled" disabled control={<Radio />} label="(Disabled option)" />*/}
						{/*</RadioGroup>*/}
					</div>
					<div className='filter__controls'>
						<Button type='submit' text='Найти' cls='filter__button filter__button--submit'/>
						<Button type='text' text='Сбросить' cls='filter__button filter__button--reset' clickHandler={() => this.resetHandler()}/>
					</div>
				</div>
				<div className="filter__result-box">
					<div className="filter__sort-box">sort</div>
					<div className="filter__result">
						{this.renderCards(this.state.filteredCards)}
					</div>
				</div>
			</form>
		);
	}
}
