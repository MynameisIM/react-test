import React from 'react';
import './loader.scss'

export default function Loader() {
	return (<div className='lds-dual-ring-container'><div className="lds-dual-ring"/></div>);
}
