import React from 'react';
import './input.scss';

export default class Input extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className={`input ${this.props.cls || ''} ${this.props.isValid ? '': 'input--error'}`}>
				<input
					type={`${this.props.type || 'text'}`}
					placeholder={this.props.placeholder}
					value={this.props.value}
					name={`${this.props.name}`}
					className='input__field'
					id={this.props.id}
					onChange={this.props.onChange}
				/>
				{this.props.isValid ? null : <span>Ошибка</span>}
				{this.props.id && this.props.label ? <label htmlFor={this.props.id}>{this.props.label}</label>: null}
			</div>
		);
	}
}
