import React from 'react';
import './list.scss';
import ListItem from "./ListItem";
import * as Api from '../../common/Fetch';

export default class List extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			cards: [
				{
					title: 'card 1',
					url: 'test.ru',
					src: './static/images/card1.jpg'
				},
				{
					title: 'card 2',
					url: 'test2.ru',
					src: './static/images/card2.jpg'
				},
				{
					title: 'card 3',
					url: 'test3.ru',
					src: './static/images/card3.jpg'
				},
				{
					title: 'card 4',
					url: 'test4.ru',
					src: './static/images/card4.jpg'
				}
			],
			page: 1,
			showLoad: true,
			url: './static/cards.json'
		};
	}

	loadCards() {
		let newCards = this.state.cards;
		Api.getData(this.state.url)
			.then(res => res.json())
			.then(data => {
				if (data.list && data.list.length) {
					if (this.state.page < data.max) {
						newCards = newCards.concat(data.list);
						this.setState({
							cards: newCards,
							page: this.state.page + 1,
						});

						if (this.state.page === data.max) {
							this.setState({
								showLoad: false,
							})
						}
					}
				}
			});
	}

	render() {
		return (
			<div className="list">
				<div className="list__wrapper">
					{this.state.cards && this.state.cards.length ?
						this.state.cards.map((item, index) => <ListItem key={index} data={item}/>): null
					}
				</div>
				{
					this.state.showLoad &&
					<div className="list__buttons-box">
						<button className="list__load" onClick={() => this.loadCards()}>Загрузить ещё</button>
					</div>
				}
			</div>
		);
	}
}
