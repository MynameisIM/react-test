import React from 'react';

function ListItem({data}) {
	return (
		<a href={data.url} className="list__item">
			<img src={data.src} alt="{props.data.title}"/>
			<span className='list__card-title'>{data.title}</span>
			<span className='list__read-more'>Читать далее</span>
		</a>
	)
}

export default ListItem;

