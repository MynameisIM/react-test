import React from 'react';
import { Link } from 'react-router-dom';
import './nav.scss';

export default function Nav () {
	return (
		<nav className='nav'>
			<ul className='nav__menu'>
				<li className='nav__item'>
					<Link to='/catalog'>
						<span className='nav__link'>Catalog</span>
					</Link>
				</li>
				<li className='nav__item'>
					<Link to='/pagination'>
						<span className='nav__link'>Test pagination</span>
					</Link>
				</li>
				<li className='nav__item'>
					<Link to='/posts'>
						<span className='nav__link'>Post page</span>
					</Link>
				</li>
				<li className='nav__item'>
					<Link to='/todo'>
						<span className='nav__link'>To Do</span>
					</Link>
				</li>
				<li className='nav__item'>
					<Link to='/articles'>
						<span className='nav__link'>Articles</span>
					</Link>
				</li>
			</ul>
		</nav>
	);
};
